import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

const userIsAuthenticated = localStorage.getItem('userIsAuthenticated') === 'true';

store.dispatch('setUser', userIsAuthenticated).then(() => {
     createApp(App).use(store).use(router).mount('#app');
});
