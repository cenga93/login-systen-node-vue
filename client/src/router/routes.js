import DashboardView from '@/views/DashboardView';
import router from '@/router/index';

export const routes = [
     {
          path: '/dashboard',
          name: 'DashboardView',
          component: /* webpackChunkName: "DashboardView" */ DashboardView,
          meta: {
               auth: true,
          },
     },
     {
          path: '/about',
          name: 'AboutView',
          component: () => /* webpackChunkName: "AboutView" */ import('../views/AboutView'),
          meta: {
               auth: true,
          },
     },
     {
          path: '/login',
          name: 'LoginView',
          component: () => /* webpackChunkName: "LoginView" */ import('../views/auth/LoginView'),
          meta: {
               auth: false,
          },
     },
     {
          path: '/register',
          name: 'RegisterView',
          component: () => /* webpackChunkName: "RegisterView" */ import('../views/auth/RegisterView'),
          meta: {
               auth: false,
          },
     },
     {
          path: '/verification/:id',
          name: 'VerifyView',
          component: () => /* webpackChunkName: "VerifyView" */ import('../views/auth/VerifyView'),
          meta: {
               auth: false,
          },
     },
     {
          path: '/reset-password',
          name: 'ResetPasswordView',
          component: () => /* webpackChunkName: "ResetPasswordView" */ import('../views/auth/ResetPasswordView'),
          meta: {
               auth: false,
          },
     },
     {
          path: '/update-password',
          name: 'UpdatePasswordView',
          component: () => /* webpackChunkName: "UpdatePasswordView" */ import('../views/auth/UpdatePasswordView'),
          meta: {
               auth: false,
          },
          beforeEnter: (to, from, next) => {
               if (to.query && to.query.token) {
                    next();
               } else {
                    router.push({ name: 'LoginView' }).then(() => {});
               }
          },
     },
     {
          path: '/:pathMatch(.*)*',
          redirect: {
               name: 'DashboardView',
          },
     },
];
