import router from '@/router';
import axios from '@/axios';
import MT from './types';

const login = async ({ commit }, payload) => {
     try {
          const response = await axios.post('/auth/login/', payload, { withCredentials: true });

          localStorage.setItem('userIsAuthenticated', 'true');

          commit(MT.SET_USER, response.data);

          await router.push({ name: 'DashboardView' });
     } catch (err) {
          console.log(err.message);
          commit(MT.SET_USER, null);
     }
};

const register = async ({ dispatch }, payload) => {
     dispatch.eee = true;

     try {
          const { status, statusText, data } = await axios.post('http://localhost:4000/api/auth/register', payload);

          if (status === 200 && statusText === 'OK') {
               await router.push({ path: `/verification/${data._id}` });
          }
     } catch (err) {
          console.log(err.message);
     }
};

const verifyUser = async ({ dispatch }, payload) => {
     dispatch.eee = true;

     try {
          const response = await axios.post(`/auth/verification/${payload.id}`, { code: payload.code });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }
     } catch (err) {
          console.log(err.message);
     }
};

const resetPassword = async ({ dispatch }, payload) => {
     dispatch.eee = true;

     try {
          const response = await axios.post(`/auth/reset-password/`, { email: payload.email });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }
     } catch (err) {
          console.log(err.message);
     }
};

const updatePassword = async ({ dispatch }, payload) => {
     dispatch.ee = true;
     try {
          const response = await axios.post(`/auth/update-password?token=${payload.token}`, { password: payload.password });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }
     } catch (err) {
          console.log(err.message);
     }
};

const setUser = async ({ commit }, data) => {
     if (data) {
          try {
               const response = await axios.get('/auth/me/', { withCredentials: true });

               commit(MT.SET_USER, response.data);
          } catch (e) {
               commit(MT.SET_USER, null);

               localStorage.setItem('userIsAuthenticated', 'false');

               await router.push({ name: 'LoginView' });
          }
     }
};

export default {
     login,
     register,
     setUser,
     verifyUser,
     resetPassword,
     updatePassword,
};
