import MT from './types';

const setUser = (state, user) => {
     state.user = user;
};

export default {
     [MT.SET_USER]: setUser,
};
