import { catchAsync } from 'catch-async-express';
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import userRepository from '@Root/repositories/user';
import { IUser } from '@Root/interfaces';

export const register = catchAsync(async (req: Request, res: Response): Promise<void> => {
     const newUser: IUser = await userRepository.createUser(req);

     res.status(httpStatus.OK).json(newUser);
});

export const verify = catchAsync(async (req: Request, res: Response): Promise<void> => {
     const _id: string = req.params.verifyId;
     const code: boolean = req.body.code;

     const verifiedUser: IUser = await userRepository.verifyUser(_id, code);

     res.status(httpStatus.OK).json(verifiedUser);
});
