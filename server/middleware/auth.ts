import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import { NextFunction, Request, Response } from 'express';
import { rolePermissions } from '@Root/config/roles';
import ApiError from '@Root/utils/ApiError.js';
import config from '@Root/config/config';

const auth = (...requiredPermissions: any[]) => (req: Request | any, res: Response, next: NextFunction) => {
    req.user = jwt.verify(req.cookies.token, config.JWT_SECRET_KEY);

    if (requiredPermissions.length) {
        const { role }: any = req.user;
        const userPermissions = rolePermissions.get(role);

        const hasRequiredPermissions = requiredPermissions.every((requiredPermission: any) => userPermissions.includes(requiredPermission));

        if (!hasRequiredPermissions) {
            throw new ApiError(httpStatus.UNAUTHORIZED, "You don't have a permission for this action");
        }
    }

    next();
};

export default auth;
