import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import { getOne, isExists } from '@Root/default';
import User, { IUserModel } from '@Root/models/User';
import Token from '@Root/models/Token';
import ApiError from '@Root/utils/ApiError';
import { sendResetPasswordMail } from '@Root/services/mailer';
import tokenRepository from './token';
import { IToken } from '@Root/interfaces';
import config from '@Root/config/config';

/** LOGIN */
const login = async (body: { email: string; password: string }): Promise<{ user: IUserModel; token: string }> => {
     const user: IUserModel = await getOne(User, { email: body.email }, { code: false });

     // Check if user exists in database or if password isn't correct
     if (!user || !(await user.comparePassword(body.password))) {
          throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
     }

     // Check if user verified
     if (!user.verified) {
          throw new ApiError(httpStatus.UNAUTHORIZED, 'Email not verified. Check your email');
     }

     // Payload for token
     const payload = {
          email: user._doc.email,
          role: user._doc.role,
     };

     const token = jwt.sign(payload, config.JWT_SECRET_KEY, { expiresIn: config.JWT_LOGIN_EXPIRATION });

     delete user._doc.password;
     delete user._doc.__v;

     return {
          user,
          token,
     };
};

/** RESET PASSWORD */
const resetPassword = async (email: string): Promise<void> => {
     const user: IUserModel = await getOne(User, { email });
     if (!user) throw new ApiError(httpStatus.NOT_FOUND, 'User not found');

     const resetToken: IToken = await tokenRepository.generateResetToken(email);

     await sendResetPasswordMail(email, resetToken.token);
};

/** UPDATE PASSWORD */
const updatePassword = async (token: string, password: any): Promise<void> => {
     jwt.verify(token, config.JWT_SECRET_KEY, async (err: any, data: any) => {
          if (err) {
               throw new ApiError(401, err.message);
          } else {
               // Check if token exists in database
               const resetToken: IToken = await isExists(Token, { token: token, blacklist: false });
               if (!resetToken) throw new ApiError(httpStatus.NOT_FOUND, 'Invalid token');

               // Check if user exists in database
               const user: IUserModel = await getOne(User, { email: data.email });
               if (!user) throw new ApiError(httpStatus.NOT_FOUND, 'User not found');

               Object.assign(user, { password });

               // Remove all reset tokens from database
               await Token.deleteMany({ $and: [{ type: 'RESET_TOKEN' }, { user_email: data.email }] });

               // Update user's password
               await user.save();
          }
     });
};

export default {
     login,
     resetPassword,
     updatePassword,
};
