import { Model } from 'mongoose';
import { IFilter, ISelect } from '@Root/interfaces';

export const isExists = async (Collection: Model<any>, filter: IFilter): Promise<any | null> => {
     return Collection.exists(filter);
};

export const getOne = async (Collection: any, filter: IFilter, notAllowedFields?: ISelect): Promise<any> => {
     return await Collection.findOne(filter).select(notAllowedFields);
};
