import { Router } from 'express';
import validate from '@Root/middleware/validation';
import * as authValidation from '@Root/routeValidations/auth.validations';
import { resetPassword, updatePassword, login, me } from '@Root/controllers/AuthController';
import { register, verify } from '@Root/controllers/UserController';
import auth from '@Root/middleware/auth';
import { Permissions } from '@Root/config/permissions';

export default () => {
     const router = Router();

     router.post('/register', validate(authValidation.register), register);
     router.post('/reset-password/', validate(authValidation.resetPassword), resetPassword);
     router.post('/update-password/', validate(authValidation.updatePassword), updatePassword);
     router.post('/login', validate(authValidation.login), login);
     router.post('/verification/:verifyId', validate(authValidation.verify), verify);
     router.get('/me', auth(Permissions.user.READ), me);

     return router;
};
