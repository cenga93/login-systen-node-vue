export interface IUser {
     _doc?: any;
     _id?: any;
     firstname: string;
     lastname: string;
     email: string;
     password?: string;
     code?: boolean;
     role: string;
     verified: boolean;
     createdAt: Date;
     updatedAt: Date;
}

export interface IFilter {
     _id?: any;
     email?: string;
     token?: string;
     code?: boolean;
     blacklist?: boolean;
}

export interface ISelect {
     _id?: number;
     code?: boolean;
     name?: number;
}

export interface IError {
     message: string;
     statusCode: number;
     isOperational?: boolean;
     stack?: string;
}

export interface IToken {
     token: string;
     createdAt: Date;
     updatedAt: Date;
}
