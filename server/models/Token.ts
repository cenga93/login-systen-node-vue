import { Model, model, Schema } from 'mongoose';
import { IToken } from '@Root/interfaces';

const UserSchema = new Schema(
     {
          token: {
               type: String,
               required: true,
          },
          type: {
               type: String,
               required: true,
          },
          user_email: {
               type: String,
               required: true,
          },
          blacklist: {
               type: Boolean,
               required: false,
               default: false,
          },
     },
     {
          timestamps: true,
     }
);

const Token: Model<IToken> = model<IToken>('Token', UserSchema);

export default Token;
